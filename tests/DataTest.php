<?php
require_once dirname(__FILE__) . '/../application/library/Data.php';
/**
 * Description of DataTest
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class DataTest extends PHPUnit_Framework_TestCase {

    public function testCreateDto() {
        $data = new Data();

        $this->assertTrue(get_class($data) === 'Data');
    }

    public function testSetDtoParameters() {
        $data = new Data();
        $data->test_key = 'test_value';

        $this->assertTrue($data->test_key === 'test_value');
    }

    public function testExportDtoDataToJson() {
        $data = new Data();
        $data->test_key1 = 'test_value1';
        $data->test_key2 = 'test_value2';

        $result = $data->toJson();
        
        $this->assertTrue($result == '{"test_key1":"test_value1","test_key2":"test_value2"}');
    }
}

