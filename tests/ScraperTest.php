<?php
require_once dirname(__FILE__) . '/../application/library/Data.php';
require_once dirname(__FILE__) . '/../application/library/Scraper.php';

/**
 * Description of ScraperTest
 * Run tests for the ScreenScraper
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class ScraperTest extends PHPUnit_Framework_TestCase {

    public function testCreateScraper() {
        $scraper = new Scraper();

        $this->assertTrue(get_class($scraper) == 'Scraper');
    }

    public function testLoadValidJsonConfig() {
        $json = '{"test_property" : "test_value"}';

        $scraper = new Scraper();
        $is_valid = $scraper->loadJsonConfig($json);

        $this->assertTrue($is_valid, "JSON Could not be loaded, it was invalid");

        $prop = $scraper->getConfigProperty('test_property');
        $this->assertTrue($prop == 'test_value');
    }

    public function testLoadInvalidJsonConfig() {
        $json = '{"test_property : "test_value"}';

        $scraper = new Scraper();
        $is_valid = $scraper->loadJsonConfig($json);

        $this->assertFalse($is_valid);
    }

    /**
     * This test is susceptible to network issues so isn't entirely reliable.
     * The url doesn't matter as long as content is returned. Could use a
     * local webserver for this test.
     */
    public function testLoadTargetPage() {
        $page = 'http://www.sainsburys.co.uk';
        $scraper = new Scraper();
        $scraper->loadTargetPage($page);

        $content = $scraper->getPage();
        $is_valid = is_string($content);

        $this->assertTrue($is_valid, "Could not load test target page");
        $this->assertRegExp('/www.sainsburys/', $content, "Failed to fetch data from remote server");
    }

    public function testParsePageForItems() {

        $content = file_get_contents(dirname(__FILE__) . '/assets/test_page.html');

        $scraper = new Scraper();
        $scraper->loadTargetHtml($content);
        $scraper->parsePageForItems();
        $items = $scraper->getItems();

        $this->assertTrue(count($items) > 0);
    }
}

