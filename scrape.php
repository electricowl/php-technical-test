<?php
$config = file_get_contents(dirname(__FILE__) . '/application/config/scraper.json');
require_once dirname(__FILE__) . '/application/library/Scraper.php';

$scraper = new Scraper();
$scraper->loadJsonConfig($config);
$scraper->run_scraper();

// Naturally the output can be directed anywhere
echo $scraper->itemSummaryToJson();
