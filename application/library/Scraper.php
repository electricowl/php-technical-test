<?php
include_once dirname(__FILE__) . '/Dom/simple_html_dom.php';
include_once dirname(__FILE__) . '/Data.php';
/**
 * Description of Scraper
 *
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Scraper {

    private $config = [];
    private $page;
    private $items = [];

    /**
     *
     * @param string $json
     * @return boolean
     */
    public function loadJsonConfig($json) {
        if (is_string($json)) {
            // Convert to an associative array (instead of stdClass)
            $config = json_decode($json, true);
        }
        // null suggests that the JSON provided is invalid
        if (null != $config) {
            $this->config = $config;
            return true;
        }
        return false;
    }

    /**
     *
     * @param string $key
     * @return mixed
     */
    public function getConfigProperty($key) {
        if (array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }
        return null;
    }

    /**
     * Load the HTML from given url
     * @param string $page url
     */
    public function loadTargetPage($page) {
        $this->page = $this->fetchUrlWithContext($page);
    }

    /**
     * Load the html (in lieu of running loadTargetPage)
     * @param string $content Html web page
     */
    public function loadTargetHtml($content) {
        $this->page = $content;
    }

    /**
     * Get the HTML of current page being processed
     * @return mixed
     */
    public function getPage() {
        return $this->page;
    }

    /**
     * Pull out the items from the page
     * There ismplenty of room here for Abstraction and Refactoring as really
     * I'd love for this to be more configuration driven.
     * @param type $criteria
     * @return array[\Data] An array of Data objects
     */
    public function parsePageForItems() {
        $target_page = new simple_html_dom();
        $target_page->load($this->page);

        foreach($target_page->find('div.product') as $item) {
            $found_item = new Data();
            // Each element in the JSON results array should contain ‘title’, ‘unit_price’, ‘size’
            // ‘description’. 'size' refers to the page weight in kb of the html requested
            $found_item->link = $item->find('a', 0)->href;
            $found_item->title = trim($item->find('a', 0)->plaintext);
            $unit_price = trim($item->find('p[class=pricePerUnit]', 0)->plaintext);
            // quick and dirty
            $found_item->unit_price = preg_replace("/[^0-9\.]+/", "", $unit_price);

            // Fetch the target link to get the description
            $product_html = $this->fetchUrlWithContext($found_item->link);
            $found_item->size = number_format(strlen($product_html)/1024, 2) . "kb";

            $product_page = new simple_html_dom();
            $product_page->load($product_html);

            $found_item->description = $product_page->find('div[id=information]', 0)->plaintext;
            $this->items[] = $found_item;
        }
    }

    public function getItems() {
        return $this->items;
    }

    public function itemSummary() {
        $summary = [];
        // fetch the results and load into the array
        $running_total = 0;

        foreach($this->items as $data) {
            $summary['results'][] = $data->toArray();
            $running_total += $data->unit_price;
        }
        $summary['total'] = $running_total;

        return $summary;
    }

    public function itemSummaryToJson() {
        return json_encode($this->itemSummary());
    }

    /**
     * Useful but not if the target server leaves off the Content-Length header
     * So much for my assumption :/
     * @param string $url Url of target page
     * @return int
     */
    public function getContentLength($url) {
        stream_context_set_default(
            array(
                'http' => array(
                    'method' => 'HEAD'
                )
            )
        );
        $headers = get_headers($url);
        return $headers['Content-Length'];
    }

    /**
     * Whilst file_get_contents() is great for many things it doesn't always request
     * files in a polite way. This function addreses that and requests the file with
     * HTTP context - ie. Headers set.
     * Note: http://php.net/manual/en/function.stream-context-create.php
     * @param string $url
     * @return string
     */
     private function fetchUrlWithContext($url) {
        // Set the stream options
        $opts = array(
          'http'=>array(
            'method'=>"GET",
            'header'=>"Accept-language: en\r\n" .
                      "Cookie: scraper=true\r\n" .
                      "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:37.0) Gecko/20100101 Firefox/37.0\r\n" .
                      "Cookie: s_ev27=%5B%5B%27Sainsburys%2520Groceries%27%2C%271355698274360%27%5D%5D; WC_PERSISTENT=oF9jgwIfjM9DMf8zkusnlTbqUL0%3d%0a%3b2015%2d04%2d24+21%3a18%3a57%2e606%5f1429906737507%2d13135%5f10151; LIST_VIEW=true; Apache=10.173.10.10.1430343602745880; JSESSIONID=0000wvU8IK8HO8yFSWbIQviv_0c:17bugodav; REFERRER=http%3a%2f%2fwww%2egoogle%2eco%2euk%2furl%3fsa%3dt%26rct%3dj%26q%3d%26esrc%3ds%26source%3dweb%26cd%3d2%26ved%3d0CCkQjBAwAQ%26url%3dhttp%253A%252F%252Fwww%2esainsburys%2eco%2euk%252Fshop%252Fgb%252Fgroceries%26ei%3drk9BVd%5ftLZXiapf1gbgG%26usg%3dAFQjCNGERP22sL7VoVoaf%5fCRUBUsM9vvlQ%26sig2%3d34Eu%2dkCKZdlnRktwPzWhZA%26bvm%3dbv%2e92189499%2cd%2ed2s; SESSION_COOKIEACCEPT=true; WC_SESSION_ESTABLISHED=true; WC_ACTIVEPOINTER=44%2c10151; WC_USERACTIVITY_103248505=103248505%2c10151%2cnull%2cnull%2cnull%2cnull%2cnull%2cnull%2cnull%2cnull%2c1A%2bWriaJnvSQwylCqZBtHyrpdASSu5BUMO9VOCcio%2fUf%2fw91rJQzXUsA8uUMzkEXYdUgDG2dwLoT%0a53BnVXB6eFwkcu6F5uucyKgXuZ5uO98aE3RY6Tw741HHRGIR8aQ1lzOFpqZsHPU7c422SQBWGHpU%0ajV8an8nmlrCiwJld9tWpwskSgL2%2b%2bFvB2NRNl0zFv0kx8QXnroE61y1jqTEmmA%3d%3d; BIGipServera-eco-www-fltc-pool-p_80=1963699466.20480.0000; sbrycookie2=Fdam5R4km; sbrycookie1=630263751; TS7d4e39=c427dff7c601edfcb1d11dbd7727fa09bc597a0d8a4e510955428dbbb4608ba71a3a9f3960ac0ec519691c965f17b450206a7c8026f49b0d3146ef5f1389de873146ef5f3fc7763435699e65b5b8ed48bfcb240e5fe41c60c53dd9c2659c44163e94063a517bec8f9e73864429848111c46e1c1be8fb08db3146ef5f; BIGipServera-eco-www-flt-pool-p_80=1712041226.20480.0000; TS7d4e39_77=3916_c365af2da66d2dba_rsb_0_rs_http%3A%2F%2Fwww.sainsburys.co.uk%2Fshop%2Fgb%2Fgroceries%2Fryvita-crispbread-fruit-crunch-200g_rs_0",
            // arbitrary 5s timeout
            'timeout' => 5,
          )
        );
        // Create the stream context
        $context = stream_context_create($opts);

        // Open the file using the HTTP headers set above
        $content = file_get_contents($url, false, $context);

        return $content;
    }

    /**
     * Instantiate the Scraper loading the config then call this method
     */
    public function run_scraper() {
        var_dump($this->config);
        $this->loadTargetPage($this->getConfigProperty('target_url'));
        $this->parsePageForItems();

    }
}

