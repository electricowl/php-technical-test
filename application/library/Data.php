<?php

/**
 * Description of Data
 * This is a simple Data Transfer Object [Fowler - PoEAA, 401]
 * Encapsulates the properties for the data, in a standard way, and allows for
 * easy serialisation when passing data across the wire.
 * @author Ian Lewis <ian.lewis@electricowl.co.uk>
 */
class Data {
    private $properties = [];

    public function toArray() {
        return $this->properties;
    }

    public function toJson() {
        return json_encode($this->properties);
    }

    public function __set($key, $value) {
        $this->properties[$key] = $value;
    }

    public function __get($key) {
        if (array_key_exists($key, $this->properties)) {
            return $this->properties[$key];
        }
        return null;
    }

    public function __unset($key) {
        unset($this->properties[$key]);
    }
}

