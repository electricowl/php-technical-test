# Programming Test #
This task is intended to test your ability to consume a webpage, process some data and
present it.

While there is no specific time limit, we would not expect you to spend any longer than 2
hours completing this.

We are looking for concise, testable, clean, well commented code and that you have
chosen the right tools for the right job. Additionally we are looking at your app structure
as a whole.

# Requirements #

Using object­ oriented PHP, build a console application​ that scrapes the grocery
site ­ Ripe Fruits page and returns a JSON array of all the products on the
page.

# Executing the Tests #

The tests are built with PHP Unit.

Git Clone the repository into an environment with PHP 5.4+ available

$ cd <project_root>

$ phpunit tests/DataTest

$ phpunit tests/ScraperTest

# Running the Application #

Git Clone the repository into an environment with PHP 5.4+ available

$ cd <project_root>

$ php ./scrape.php

The application will run and output the json to the console.

# Configuration #

This is currently in application/config/scraper.json

However, an effective solution for configuration that is very flexible would be etcd.

Written in Golang and providing a responsive k/v store in JSON natively.